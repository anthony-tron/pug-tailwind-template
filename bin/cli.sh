# set $manager to available package manager
yarn -v > /dev/null && manager=yarn || manager=npm

if [ -z $1 ] # if directory is supplied
then
    echo "usage: $(basename $0) project_name"
    exit 1
elif [ -d $1 ] # if valid directory
then
    echo "$(basename $0): $1: File exists"
    exit 1
elif ! git clone -b master --single-branch --depth 1 https://gitlab.com/anthony-tron/create-pug-tailwind-starter $1
then
    echo "$(basename $0): Failed when clonning repository"
    exit 1
elif ! (cd $1 && eval $manager install)
then
    echo "$(basename $0): Failed when installing dependencies"
    exit 1
fi
# All mandatory commands have been executed


cd $1

# Optional:
# Update package.json
if ! ( (yarn global add npe ) || (npm list -g npe || npm install -g npe) \
    && npe name "$1" \
    && npe description "" \
    && npe bin "" \
    && npe license "" \
    && npe author $(git config --global user.name) \
    && npe version "0.1.0" \
    && npe repository "" \
    && npe homepage "" \
    && npe keywords "pug tailwindcss" )
then
    echo "$(basename $0): WARNING: Failed editing package.json"
fi

# Optional:
# Clean bin/ and LICENSE
if ! (rm -r bin LICENSE)
then
    echo "$(basename $0): WARNING: Failed cleaning $1"
fi

# Optional:
# Remove unused lock file
if [ "$manager" = "npm" ]
then
    rm yarn.lock
else
    rm package-lock.json
fi

# Optional:
# Create git repository
if ! ( (yes | rm -r .git) && git init && git add --all && git commit -m "🎉 Initialized with $(basename $0)" )
then
    echo "$(basename $0): WARNING: Failed creating a git repository"
fi

echo
echo "Thank you for creating a new project with $(basename $0)!"
echo "Get started:"
echo "  cd $1"
echo "  $manager start"
echo
