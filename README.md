# create-pug-tailwind-starter

Take advantage of modern technologies to build fluently static websites.

[→ Have a look at what you can build ←](https://anthony-tron.gitlab.io/create-pug-tailwind-starter/)

This projects uses
- [live-server](https://www.npmjs.com/package/live-server) for hot reload
- [tailwindcss](https://www.npmjs.com/package/tailwindcss)
- [pug](https://www.npmjs.com/package/pug)

## Get started 🚀

Replace `my-app` with your project name as it will create your package.json with it.

```sh
npx create-pug-tailwind-starter my-app
cd my-app
npm start
```

Now ready to dev.


## Scripts 📃

Scripts will write in the `build` directory.

- `npm start`: Start development server. Pages will reload on save.
- `npm buid:production`: Build and optimize pages for production.

- `npm build:html`: builds pug into html without prettifying
- `npm build:css`: builds css without purging
- `npm build:dev`: build pug into pretty html and css without purging


## Production 🏭

**`live-server` is not recommanded to deploy this on your production server.** Instead, please use an alternative like [http-server](https://www.npmjs.com/package/http-server), [serve-static](https://www.npmjs.com/package/serve-static), [server](https://www.npmjs.com/package/server).
